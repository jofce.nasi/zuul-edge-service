#!/bin/bash

cd app-soa
mvn package docker:build -Dmaven.test.skip=true
cd ..

cd eureka
mvn package docker:build -DMaven.test.skip=true
cd ..

cd soa-company-data
mvn package docker:build -DMaven.test.skip=true
cd ..

cd zuul
mvn package docker:build -DMaven.test.skip=true
cd ..
